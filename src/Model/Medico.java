package Model;

public class Medico extends Usuario{

	private String registroMedico;
	private String especialidade;
    private ConsultaMedica umaConsultaMedica;
	
	
	public Medico(String nome, String especialidade) {
		super(nome);
		this.especialidade  = especialidade;
	}

	public String getRegistroMedico() {
		return registroMedico;
	}

	public void setRegistroMedico(String registroMedico) {
		this.registroMedico = registroMedico;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public ConsultaMedica getUmaConsultaMedica() {
		return umaConsultaMedica;
	}

	public void setUmaConsultaMedica(ConsultaMedica umaConsultaMedica) {
		this.umaConsultaMedica = umaConsultaMedica;
	}


	

}
