package Model;

public class Paciente extends Usuario {
	
	private String idade;
	private String sintoma;
	private String planoSaude;
	private ConsultaMedica umaConsultaMedica;

	
	
	public String getPlanoSaude() {
		return planoSaude;
	}

	public void setPlanoSaude(String planoSaude) {
		this.planoSaude = planoSaude;
	}
	
	public String getSintoma() {
		return sintoma;
	}
	public void setSintoma(String sintoma) {
		this.sintoma = sintoma;
	}
	
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	
	public Paciente(String nome, String sintoma) {
		super(nome);
		this.sintoma = sintoma;
	}
	public ConsultaMedica getUmaConsultaMedica() {
		return umaConsultaMedica;
	}
	public void setUmaConsultaMedica(ConsultaMedica umaConsultaMedica) {
		this.umaConsultaMedica = umaConsultaMedica;
	}
	

}
