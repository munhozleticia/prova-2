package Controllers;

import java.util.ArrayList;

import Model.ConsultaMedica;
import Model.Medico;
import Model.Paciente;


public class Controller {

	final ArrayList<Paciente> listaPaciente;
	final ArrayList<Medico> listaMedico;

	public Controller() {
		listaPaciente = new ArrayList<Paciente>();
		listaMedico = new ArrayList<Medico>();	
	}
	
	
	public void adicionar(Medico umMedico) {
		
		listaMedico.add(umMedico);
	}
	
	
	public void adicionar(Paciente umPaciente) {
		listaPaciente.add(umPaciente);
	}
	
	public void remover(Medico umMedico ) {
		listaMedico.remove(umMedico);
	}
	
	public void remover(Paciente umPaciente) {
		listaPaciente.remove(umPaciente);
	}
	
	public Medico buscarMedico(String umNome) {
		if(listaMedico.size()!=0) {
			for (Medico umMedico : listaMedico) {
				if (umMedico.getNome().equalsIgnoreCase(umNome)==true){
					return umMedico;
				}
			}
		}
		System.out.println("Não foi cadastrado nenhum médico.");
		return null;
	}
	
	public Paciente buscarPaciente(String nome) {
		for (Paciente umPaciente : listaPaciente) {
			if (umPaciente.getNome().equalsIgnoreCase(nome)==true){
				return umPaciente;
			}
		}
		System.out.println("Não foi cadastrado nenhum paciente.");
		return null;
		
	}

	public String listarUsuarios() {
		String UsuarioCadastrados = "Medico";
		for(Medico umMedico : listaMedico) {
			UsuarioCadastrados = (UsuarioCadastrados + "\n" + umMedico.getNome());
		}
		UsuarioCadastrados = (UsuarioCadastrados + "\n\n" + "Paciente");
		for(Paciente umPaciente : listaPaciente) {
			UsuarioCadastrados = (UsuarioCadastrados + "\n" + umPaciente.getNome());
		}
		return UsuarioCadastrados;
	} 
	
	public String fichaUsuario (Medico umMedico) {
		ConsultaMedica umaConsultaMedica = umMedico.getUmaConsultaMedica();
		String stringGigante = ("Medico" + "\n" + "Nome : " + umMedico.getNome() + "\n" +
								"Especialidade : " + umMedico.getEspecialidade() + "\n" + "RG : " + umMedico.getRg() + "\n" +
								"Data : " + umaConsultaMedica.getData() + "\n" + "Horario : " + umaConsultaMedica.getHorario() + "\n" + 
								"Diagnostico : " + umaConsultaMedica.getDiagnostico() + "\n" + "CPF : " + umMedico.getCpf() + "\n" + 
								"Registro Medico : " + umMedico.getRegistroMedico() );
		return stringGigante;
	}
	
	public String fichaUsuario (Paciente umPaciente) {
		ConsultaMedica umaConsultaMedica = umPaciente.getUmaConsultaMedica();
		String stringGigante = ("Paciente" + "\n" + "Nome : " + umPaciente.getNome() + "\n" +
								"Sintoma : " + umPaciente.getIdade() + "\n" + "RG : " + umPaciente.getRg() + "\n" +
								"Data : " + umaConsultaMedica.getData() + "\n" + "Horário : " + umaConsultaMedica.getHorario() + "\n" + 
								"Diagnostico : " + umaConsultaMedica.getDiagnostico() + "\n" + "CPF : " + umPaciente.getCpf() + "\n" +
								"Plano de Saude : " + umPaciente.getPlanoSaude() + "\n" +  "Idade : " + umPaciente.getIdade() );
		return stringGigante;
	}

}

	